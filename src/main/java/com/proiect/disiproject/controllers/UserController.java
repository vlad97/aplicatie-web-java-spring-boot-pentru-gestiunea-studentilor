package com.proiect.disiproject.controllers;

import com.proiect.disiproject.converters.StudentConverter;
import com.proiect.disiproject.data.UserType;
import com.proiect.disiproject.dto.StudentDTO;
import com.proiect.disiproject.entities.Catalog;
import com.proiect.disiproject.entities.Secretary;
import com.proiect.disiproject.entities.Student;
import com.proiect.disiproject.entities.Teacher;
import com.proiect.disiproject.service.CatalogService;
import com.proiect.disiproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    public static final String LOGGED_USER = "loggedUser";
    public static final String USER_TYPE = "userType";

    private UserService userService;
    private StudentConverter studentConverter;
    private CatalogService catalogService;

    @Autowired
    public UserController(UserService userService, StudentConverter studentConverter, CatalogService catalogService) {
        this.userService = userService;
        this.studentConverter = studentConverter;
        this.catalogService = catalogService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    String getRegistrationPage(Model model) {
        model.addAttribute("mismatchPasswords", Boolean.FALSE);
        model.addAttribute("notRegisteredBySecretary", Boolean.FALSE);
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    String register(Model model, @RequestParam(value = "mail") String mail, @RequestParam(value = "password1") String password1, @RequestParam(value = "password2") String password2, @RequestParam(value = "userType") String userType, @RequestParam(value = "username") String username) {
        if (UserType.Teacher.toString().equals(userType)) {
            Teacher teacher = userService.getTeacherByMail(mail);
            if (isUserValid(teacher, model, password1, password2)) {
                teacher.setUsername(username);
                teacher.setPassword(password1);
                userService.updateTeacher(teacher);
                return "redirect:login";
            }
        } else {
            if (UserType.Student.toString().equals(userType)) {
                Student student = userService.getStudentByMail(mail);
                if (isUserValid(student, model, password1, password2)) {
                    student.setUsername(username);
                    student.setPassword(password1);
                    userService.updateStudent(student);
                    return "redirect:login";
                }
            }
        }
        return "register";
    }

    private Boolean isUserValid(Object user, Model model, String password1, String password2) {
        if (user == null) {
            model.addAttribute("mismatchPasswords", Boolean.FALSE);
            model.addAttribute("notRegisteredBySecretary", Boolean.TRUE);
            return Boolean.FALSE;
        }
        if (!password1.equals(password2)) {
            model.addAttribute("mismatchPasswords", Boolean.TRUE);
            model.addAttribute("notRegisteredBySecretary", Boolean.FALSE);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    String getLoginPage(Model model) {
        model.addAttribute("invalidLogin", Boolean.FALSE);
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    String doLogin(Model model, @RequestParam(value = "username") String username, @RequestParam(value = "password") String password, @RequestParam(value = "userType") String userType, HttpSession session) {
        if (UserType.Teacher.toString().equals(userType)) {
            Teacher teacher = userService.getTeacherByUsername(username);
            if (teacher == null) {
                model.addAttribute("invalidLogin", Boolean.TRUE);
                return "login";
            }
            if (teacher.getPassword() != null && !teacher.getPassword().equals(password)) {
                model.addAttribute("invalidLogin", Boolean.TRUE);
                return "login";
            }
            session.setAttribute(LOGGED_USER, teacher.getUsername());
            session.setAttribute(USER_TYPE, UserType.Teacher.toString());
            return "teacher-menu";
        } else {
            if (UserType.Student.toString().equals(userType)) {
                Student student = userService.getStudentByUsername(username);
                if (student == null) {
                    model.addAttribute("invalidLogin", Boolean.TRUE);
                    return "login";
                }
                if (student.getPassword() != null && !student.getPassword().equals(password)) {
                    model.addAttribute("invalidLogin", Boolean.TRUE);
                    return "login";
                }
                session.setAttribute(LOGGED_USER, student.getUsername());
                session.setAttribute(USER_TYPE, UserType.Student.toString());
                return "redirect:user-details-page";
            } else {
                if (UserType.Secretary.toString().equals(userType)) {
                    Secretary secretary = userService.getSecretaryByUsername(username);
                    if (secretary == null) {
                        model.addAttribute("invalidLogin", Boolean.TRUE);
                        return "login";
                    }
                    if (secretary.getPassword() != null && !secretary.getPassword().equals(password)) {
                        model.addAttribute("invalidLogin", Boolean.TRUE);
                        return "login";
                    }
                    session.setAttribute(LOGGED_USER, secretary.getUsername());
                    session.setAttribute(USER_TYPE, UserType.Secretary.toString());
                    return "redirect:secretary-menu";
                }
            }
        }
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    String doLogout(Model model, HttpSession session) {
        session.removeAttribute(LOGGED_USER);
        session.removeAttribute(USER_TYPE);
        return "redirect:login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    String doLogoutPost(Model model, HttpSession session) {
        session.removeAttribute(LOGGED_USER);
        session.removeAttribute(USER_TYPE);
        return "redirect:login";
    }

    @RequestMapping(value = "/addStudent", method = RequestMethod.GET)
    String getAddStudentPage(Model model, HttpSession session) {
        String userType = (String) session.getAttribute(USER_TYPE);
        if (userType != null && UserType.Secretary.toString().equals(userType)) {
            model.addAttribute("userExists", Boolean.FALSE);
            return "addStudent";
        }
        return "redirect:login";
    }

    @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
    String addStudent(Model model, @RequestParam(value = "mail") String mail, @RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName,@RequestParam(value = "groupNumber") Integer groupNumber, HttpSession session) {
        String userType = (String) session.getAttribute(USER_TYPE);
        if (userType != null && UserType.Secretary.toString().equals(userType)) {
            Student student = userService.getStudentByMail(mail);

            if (student != null) {
                System.out.println("got here");
                model.addAttribute("userExists", Boolean.TRUE);
                return "addStudent";
            } else {
                student = new Student();
                student.setFirstName(firstName);
                student.setLastName(lastName);
                student.setMail(mail);
                student.setGroupNumber(groupNumber);
                userService.addStudent(student);
                return "redirect:secretary-menu";
            }
        }
        return "redirect:login";
    }


    @RequestMapping(value = "/addTeacher", method = RequestMethod.GET)
    String getAddTeacherPage(Model model, HttpSession session) {
        String userType = (String) session.getAttribute(USER_TYPE);
        System.out.println(userType);
        if (userType != null && UserType.Secretary.toString().equals(userType)) {
            model.addAttribute("teacherExists", Boolean.FALSE);
            return "addTeacher";
        }
        return "redirect:login";
    }

    @RequestMapping(value = "/addTeacher", method = RequestMethod.POST)
    String addTeacher(Model model, @RequestParam(value = "mail") String mail, @RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName, HttpSession session) {
        String userType = (String) session.getAttribute(USER_TYPE);
        System.out.println(userType);
        if (userType != null && UserType.Secretary.toString().equals(userType)) {
            Teacher teacher = userService.getTeacherByMail(mail);
            if (teacher != null) {
                System.out.println("got here");
                model.addAttribute("teacherExists", Boolean.TRUE);
                return "addTeacher";
            } else {
                teacher = new Teacher();
                teacher.setFirstName(firstName);
                teacher.setLastName(lastName);
                teacher.setMail(mail);
                userService.addTeacher(teacher);
                return "redirect:secretary-menu";
            }
        }
        return "redirect:login";
    }


    @RequestMapping(value = "/user-details-page", method = RequestMethod.GET)
    String getUserDetailsPage(Model model, HttpSession session) {
        Student loggedStudent = getLoggedStudent(session);
        if (loggedStudent != null) {
            model.addAttribute("student", loggedStudent);
            return "user-details-page";
        }
        return "redirect:login";
    }

    @RequestMapping(value = "/user-details-page", method = RequestMethod.POST)
    String updateUserDetails(@ModelAttribute("student") StudentDTO studentDTO, Model model) {
        Student student = studentConverter.convertToStudent(studentDTO);
        userService.updateStudent(student);
        model.addAttribute("student", student);
        model.addAttribute("successfullyUpdated", Boolean.TRUE);
        return "user-details-page";
    }

    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    String getCatalogPage(Model model, HttpSession session) {
        Student loggedStudent = getLoggedStudent(session);
        if (loggedStudent != null) {
            model.addAttribute("catalog", catalogService.getCatalogForStudent(loggedStudent.getId()));
            return "student-catalog-page";
        }

        return "redirect:login";
    }

    private Student getLoggedStudent(HttpSession session) {
        String userType = (String)session.getAttribute(USER_TYPE);
        if (userType != null && UserType.Student.toString().equals(userType)) {
            String username = (String) session.getAttribute(LOGGED_USER);
            if (username != null) {
                return userService.getStudentByUsername(username);
            }
        }
        return null;
    }

    @RequestMapping(value = "/secretary-menu", method = RequestMethod.GET)
    String getSecretaryMenu(Model model, HttpSession session) {
        String userType = (String) session.getAttribute(USER_TYPE);
        if (userType != null && UserType.Secretary.toString().equals(userType)) {
            String username = (String) session.getAttribute(LOGGED_USER);
            if (username != null) {
                Secretary loggedSecretary = userService.getSecretaryByUsername(username);
                model.addAttribute("secretary", loggedSecretary);
                return "secretary-menu";
            }
        }
        return "redirect:logout";
    }

    @RequestMapping(value = "/teacher-menu", method = RequestMethod.GET)
    String getTeacherMenu(Model model, HttpSession session) {
        String userType = (String) session.getAttribute(USER_TYPE);
        if (userType != null && UserType.Teacher.toString().equals(userType)) {
            String username = (String) session.getAttribute(LOGGED_USER);
            if (username != null) {
                Teacher loggedSecretary = userService.getTeacherByUsername(username);
                model.addAttribute("teacher", loggedSecretary);
                return "teacher-menu";
            }
        }
        return "redirect:logout";
    }

    @RequestMapping(value = "/manage-students", method = RequestMethod.GET)
    String listStudents(Model model, HttpSession session) {
        String userType = (String) session.getAttribute(USER_TYPE);
        if (userType != null && (UserType.Secretary.toString().equals(userType) || UserType.Teacher.toString().equals(userType))) {
            String username = (String) session.getAttribute(LOGGED_USER);
            if (username != null) {
                List<Student> studentsList = userService.getAllStudents();
                model.addAttribute("studentsList", studentsList);
                return "manage-students";
            }
        }
        return "redirect:logout";
    }

    @RequestMapping(value = "/manage-students", method = RequestMethod.POST)
    String listStudents(@ModelAttribute("student") StudentDTO studentDTO, Model model) {
        List<Student> studentsList = userService.getAllStudents();
        model.addAttribute("studentsList", studentsList);
        return "redirect:manage-students";
    }

    @RequestMapping(value = "manage-students/delete/{id}", method = RequestMethod.GET)
    public String deleteAccount(@PathVariable Integer id, HttpSession session, final RedirectAttributes redirectAttributes) {
        String username = (String) session.getAttribute(LOGGED_USER);
        if (username == null)
            return "login";

        Student student = userService.deleteSrudent(id);
        String message = "The student  " + student.getFirstName() + " " + student.getLastName() + " was successfully removed from database.";
        redirectAttributes.addFlashAttribute("message", message);

        return "redirect:/manage-students";
    }



    @RequestMapping(value = "manage-students/edit/{id}", method = RequestMethod.GET)
    public String editStudentPage(@PathVariable Integer id, HttpSession session, Model model) {
        String username = (String) session.getAttribute(LOGGED_USER);
        if (username == null)
            return "login";

        Student student = userService.findStudentById(id);

        model.addAttribute("student", student);
        return "edit-student";
    }

    @RequestMapping(value = "manage-students/edit/{id}", method = RequestMethod.POST)
    public String editStudent(@ModelAttribute @Valid Student student,
                              BindingResult result,
                              @PathVariable Integer id, HttpSession session,
                              final RedirectAttributes redirectAttributes) {

        String username = (String) session.getAttribute(LOGGED_USER);
        if (username == null)
            return "login";

        String message = "User was successfully updated.";
        userService.updateStudent(student);


        redirectAttributes.addFlashAttribute("message", message);
        return "redirect:/manage-students";
    }

    @RequestMapping(value = "manage-students/catalog/{id}", method = RequestMethod.GET)
    public String getCatalog(@PathVariable Integer id, HttpSession session, Model model) {
        String userType = (String) session.getAttribute(USER_TYPE);
        if (userType != null && UserType.Teacher.toString().equals(userType)) {
            Student student = userService.findStudentById(id);
            model.addAttribute("student", student);
            List<Catalog> catalogsList=userService.getStudentMarks(student.getId());
            model.addAttribute("catalogList", catalogsList);
            return "catalog";
        }
        return "redirect:logout";
    }

}
