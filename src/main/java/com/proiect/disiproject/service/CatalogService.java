package com.proiect.disiproject.service;

import com.proiect.disiproject.data.CatalogData;

import java.util.List;

public interface CatalogService {

    List<CatalogData> getCatalogForStudent(int studentID);
}
