package com.proiect.disiproject.service.impl;

import com.proiect.disiproject.dao.CatalogDAO;
import com.proiect.disiproject.dao.SecretaryDAO;
import com.proiect.disiproject.dao.StudentDAO;
import com.proiect.disiproject.dao.TeacherDAO;
import com.proiect.disiproject.entities.*;
import com.proiect.disiproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private StudentDAO studentDAO;
    private TeacherDAO teacherDAO;
    private SecretaryDAO secretaryDAO;
    private CatalogDAO catalogDAO;

    @Autowired
    public UserServiceImpl(StudentDAO studentDAO, TeacherDAO teacherDAO, SecretaryDAO secretaryDAO, CatalogDAO catalogDAO) {
        this.studentDAO = studentDAO;
        this.teacherDAO = teacherDAO;
        this.secretaryDAO=secretaryDAO;
        this.catalogDAO=catalogDAO;
    }

    public List<Student> getAllStudents() {
        return studentDAO.findAll();
    }

    @Override
    public Student getStudentByMail(String mail) {
        return studentDAO.findByMail(mail);
    }

    @Override
    public Student getStudentByGroupNumber(Integer groupNumber) {
        return studentDAO.findByGroupNumber(groupNumber);
    }

    

    @Override
    public void updateStudent(Student student) {
        studentDAO.save(student);
    }


    @Override
    public void addStudent(Student student) {
        studentDAO.save(student);
    }


    @Override
    public Student getStudentByUsername(String username) {
        return studentDAO.findByUsername(username);
    }

    @Override
    public Teacher getTeacherByMail(String mail) {
        return teacherDAO.findByMail(mail);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherDAO.save(teacher);
    }

    @Override
    public void addTeacher(Teacher teacher) {
        teacherDAO.save(teacher);
    }

    @Override
    public Teacher getTeacherByUsername(String username) {
        return teacherDAO.findByUsername(username);
    }

    @Override
    public Secretary getSecretaryByUsername(String username) {
        return secretaryDAO.findByUsername(username);
    }

    @Override
    public Student deleteSrudent(Integer id) {
        Student deleteStudent= studentDAO.findStudentById(id);
        studentDAO.delete(deleteStudent);

        return deleteStudent;
    }




    @Override
    public Student findStudentById(Integer id) {

        return studentDAO.findStudentById(id);
    }

    @Override
    public List<Catalog> getStudentMarks(int id) {
        return catalogDAO.getMarksByStudentID(id);
    }
}
