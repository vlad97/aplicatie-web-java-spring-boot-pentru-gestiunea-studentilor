package com.proiect.disiproject.service.impl;

import com.proiect.disiproject.dao.CatalogDAO;
import com.proiect.disiproject.data.CatalogData;
import com.proiect.disiproject.entities.Catalog;
import com.proiect.disiproject.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CatalogServiceImpl implements CatalogService {

    private CatalogDAO catalogDAO;

    @Autowired
    public CatalogServiceImpl(CatalogDAO catalogDAO) {
        this.catalogDAO = catalogDAO;
    }

    @Override
    public List<CatalogData> getCatalogForStudent(int studentID) {
        List<Catalog> catalogList = catalogDAO.getMarksByStudentID(studentID);
        List<CatalogData> catalogDataList = new ArrayList<>();
        int id = 1;
        for (Catalog catalog : catalogList) {
            CatalogData catalogData = new CatalogData();
            catalogData.setId(id);
            catalogData.setMark(catalog.getMark());
            catalogData.setSubject(catalog.getSubjectName());
            catalogData.setDate(catalog.getMarkDate());
            catalogDataList.add(catalogData);
            id++;
        }

        return catalogDataList;
    }
}
