package com.proiect.disiproject.service;

import com.proiect.disiproject.entities.Catalog;
import com.proiect.disiproject.entities.Secretary;
import com.proiect.disiproject.entities.Student;
import com.proiect.disiproject.entities.Teacher;

import java.util.List;

public interface UserService {
    List<Student> getAllStudents();

    Student getStudentByMail(String mail);

    Student getStudentByGroupNumber(Integer groupNumber);

    void updateStudent(Student student);

    void addStudent(Student student);

    Student getStudentByUsername(String username);

    Teacher getTeacherByMail(String mail);

    void updateTeacher(Teacher teacher);

    void addTeacher(Teacher teacher);

    Teacher getTeacherByUsername(String username);

    Secretary getSecretaryByUsername(String username);

    Student deleteSrudent(Integer id);


    Student findStudentById(Integer id);

    List<Catalog> getStudentMarks(int id);
}
