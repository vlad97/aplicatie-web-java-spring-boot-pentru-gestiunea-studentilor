package com.proiect.disiproject.dao;

import com.proiect.disiproject.entities.Secretary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecretaryDAO extends JpaRepository<Secretary, Integer> {

    Secretary findByUsername(String mail);
}