package com.proiect.disiproject.dao;

import com.proiect.disiproject.entities.Catalog;
import com.proiect.disiproject.entities.Secretary;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CatalogDAO extends JpaRepository<Catalog, Integer> {
    List<Catalog> getMarksByStudentID(int id);
}
