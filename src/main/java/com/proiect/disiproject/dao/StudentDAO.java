package com.proiect.disiproject.dao;

import com.proiect.disiproject.entities.Student;
import com.proiect.disiproject.entities.StudentGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentDAO extends JpaRepository<Student, Integer> {

    Student findByMail(String mail);
    Student findByGroupNumber(Integer groupNumber);
    Student findStudentById(Integer id);

    Student findByUsername(String mail);



    void delete(Student deleteStudent);



}
