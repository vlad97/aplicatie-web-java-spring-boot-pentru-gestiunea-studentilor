package com.proiect.disiproject.dao;

import com.proiect.disiproject.entities.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherDAO extends JpaRepository<Teacher, Integer> {

    Teacher findByMail(String mail);

    Teacher findByUsername(String username);
}
