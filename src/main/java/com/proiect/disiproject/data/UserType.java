package com.proiect.disiproject.data;

public enum UserType {
    Teacher,
    Student,
    Secretary
}
