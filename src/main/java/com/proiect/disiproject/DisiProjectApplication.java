package com.proiect.disiproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class DisiProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(DisiProjectApplication.class, args);
    }

}
