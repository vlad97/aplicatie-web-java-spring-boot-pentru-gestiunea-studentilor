package com.proiect.disiproject.converters;

import com.proiect.disiproject.dao.StudentDAO;
import com.proiect.disiproject.dto.StudentDTO;
import com.proiect.disiproject.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentConverter {

    @Autowired
    StudentDAO studentDAO;

    public Student convertToStudent(StudentDTO studentDTO) {
        Student student = studentDAO.findByUsername(studentDTO.getUsername());
        student.setMail(studentDTO.getMail());
        student.setGroupNumber(studentDTO.getGroupID());
        student.setPhone(studentDTO.getPhone());
        return student;
    }
}
