<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<html>
<head>
    <title>AddStudent</title>
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
<div class="main-container">

    <form method="post">
        <div class="info">
            <h3> Enter the new student name and email</h3>
        </div>
        <div class="add-student">
            <label for="firstName"><b>First name:</b></label>
            <input type="firstName" name="firstName" id="firstName" required>

            <label for="lastName"><b>Last name:</b></label>
            <input type="lastName" name="lastName" id="lastName" required>

            <label for="mail"><b>Mail Address:</b></label>
            <input type="mail" name="mail" id="mail" required>

            <label for="groupNumber"><b>Group Number:</b></label>
            <input type="groupNumber" name="groupNumber" id="groupNumber" required>


            <button type="submit" id="add-button">Add</button>
            <button onclick="goBack()">Go Back</button>
        </div>
    </form>
    <c:if test="${userExists eq true}">
        <div>
            <p class="error"> The student is already registered!</p>
        </div>
    </c:if>

</div>
</body>
</html>
