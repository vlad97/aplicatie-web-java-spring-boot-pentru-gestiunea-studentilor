<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<html>
<head>
    <title>Accounts overview</title>
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <h1>Edit Student</h1>

    <%--@elvariable id="student" type="java"--%>
    <form:form class="form-horizontal" method="POST" modelAttribute="student" action="${pageContext.request.contextPath}/manage-students/edit/${student.id}">

        <form:hidden path="id" />
        <form:hidden path="password" />

        <spring:bind path="firstName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">First name</label>
                <div class="col-sm-10">
                    <form:input path="firstName" type="text" class="form-control " id="firstName" placeholder="First name" value="${student.firstName}" />
                    <form:errors path="firstName" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="lastName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Last name</label>
                <div class="col-sm-10">
                    <form:input path="lastName" type="text" class="form-control " id="firstName" placeholder="Last name" value="${student.lastName}" />
                    <form:errors path="lastName" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="username">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <form:input path="username" type="text" class="form-control " id="username" placeholder="Username" value="${student.username}" />
                    <form:errors path="username" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="mail">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <form:input path="mail" type="text" class="form-control " id="mail" placeholder="Email" value="${student.mail}" />
                    <form:errors path="mail" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="phone">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Phone number</label>
                <div class="col-sm-10">
                    <form:input path="phone" type="text" class="form-control " id="phone" placeholder="Phone number" value="${student.phone}" />
                    <form:errors path="phone" class="control-label" />
                </div>
            </div>
        </spring:bind>

        <spring:bind path="groupNumber">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label class="col-sm-2 control-label">Group number</label>
                <div class="col-sm-10">
                    <form:input path="groupNumber" type="text" class="form-control " id="mail" placeholder="Group number" value="${student.groupNumber}" />
                    <form:errors path="groupNumber" class="control-label" />
                </div>
            </div>
        </spring:bind>



        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn-lg btn-primary pull-right" onclick="location.href='${pageContext.request.contextPath}/manage-students/edit/${student.id}'">Edit</button>

            </div>
        </div>

    </form:form>

</div>
<button onclick="location.href='${pageContext.request.contextPath}/manage-students'">Go Back</button>
</body>
</html>
