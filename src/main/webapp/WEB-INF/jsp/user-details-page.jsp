<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>User Details</title>
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="header">
    <h3> Personal info</h3>
    <div class="header-right">
        <a href="/catalog"> See catalog </a>
    </div>
</div>

<div class="personal-info-container">
    <form:form method="post" action="/user-details-page" modelAttribute="student">

        <c:if test="${successfullyUpdated eq true}">
            <div class="alert alert-success" role="alert">
                <p> Successfully updated</p>
            </div>
        </c:if>
        <div class="update-profile-form">
            <label><b>Username:</b></label>
            <form:input path="username" type="text" id="username" class="readonly-input" value="${student.username}"
                        readonly="true"/>

            <label><b>First name:</b></label>
            <form:input path="firstName" type="text" id="firstName" class="readonly-input" value="${student.firstName}"
                        readonly="true"/>

            <label><b>Last Name:</b></label>
            <form:input path="lastName" type="text" id="lastName" class="readonly-input" value="${student.lastName}"
                        readonly="true"/>

            <label><b>Group:</b></label>
            <form:input path="groupNumber" type="text" id="groupNumber" class="readonly-input" value="${student.groupNumber}"
                        readonly="true"/>

            <label><b>Mail Address:</b></label>
            <form:input path="mail" type="text" id="mail" value="${student.mail}"/>

            <label><b>Phone number:</b></label>
            <form:input path="phone" type="text" id="phone" value="${student.phone}"/>

            <button type="submit" id="save-profile-updates">Save Changes</button>

        </div>
    </form:form>
</div>

</body>
</html>
