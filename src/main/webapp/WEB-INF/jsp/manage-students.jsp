<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<html>
<head>
    <title>Accounts overview</title>
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="main-container">
    <% if(request.getSession().getAttribute("userType").equals("Secretary")){%>
    <c:if test="${not empty message}">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${message}</strong>
        </div>
    </c:if>

    <h1>All Students</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Group number</th>
            <th>Action</th>
        </tr>
        </thead>
        <c:forEach var="student" items="${studentsList}">
            <tr>
                <td>${student.firstName}</td>
                <td>${student.lastName}</td>
                <td>${student.groupNumber}</td>
                <spring:url value="/manage-students/edit/${student.id}" var="updateUrl" />



                <td>
                    <button class="btn btn-warning" onclick="location.href='${updateUrl}'">Update</button>
                    <button class="btn btn-danger" onclick="location.href='/manage-students/delete/${student.id}'">Delete</button></td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
    <button onclick="location.href='/secretary-menu'">Go Back</button>
    <%} if(request.getSession().getAttribute("userType").equals("Teacher")){%>
    <h1>All Students</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Group number</th>
            <th>Action</th>
        </tr>
        </thead>
        <c:forEach var="student" items="${studentsList}">
            <tr>
                <td>${student.firstName}</td>
                <td>${student.lastName}</td>
                <td>${student.groupNumber}</td>
                <spring:url value="/manage-students/catalog/${student.id}" var="catalogUrl" />

                <td>
                    <button class="btn btn-warning" onclick="location.href='${catalogUrl}'">Catalog</button>
            </tr>
        </c:forEach>
        </tbody>

    </table>

    <button onclick="location.href='/teacher-menu'">Go Back</button>
    <%}%>
</div>


</body>
</html>
