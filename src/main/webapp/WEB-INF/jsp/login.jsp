<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Login</title>
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
<div class="main-container">

    <form method="post">
        <div class="info">
            <h3> Please enter your credentials</h3>
        </div>
        <div class="login-form">
            <label for="username"><b>Username:</b></label>
            <input type="username" name="username" id="username" required>

            <label for="password"><b>Password:</b></label>
            <input type="password" name="password" id="password" required>

            <select class="dropdown" name="userType">
                <option>Student</option>
                <option>Teacher</option>
                <option>Secretary</option>

            </select>

            <button type="submit" id="login-button">Login</button>

        </div>
    </form>
    <button onclick="location.href='/register'">Register</button>
    <c:if test="${invalidLogin eq true}">
        <div>
            <p class="error"> Username or password is incorrect!</p>
        </div>
    </c:if>

</div>
</body>
</html>
