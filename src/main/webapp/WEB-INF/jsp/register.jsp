<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Register</title>
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
<div class="main-container">

    <form method="post">
        <div class="info">
            <h3> Please enter the mail provided by the secretary and chose a password</h3>
        </div>
        <div class="register-form">
            <label for="mail"><b>Mail Address:</b></label>
            <input type="username" name="mail" id="mail" required>

            <label for="username"><b>Username:</b></label>
            <input type="username" name="username" id="username" required>

            <label for="password1"><b>Password:</b></label>
            <input type="password" name="password1" id="password1" required>

            <label for="password2"><b>Retype the password:</b></label>
            <input type="password" name="password2" id="password2" required>

            <select class="dropdown" name="userType">
                <option>Student</option>
                <option>Teacher</option>
            </select>

            <button type="submit" id="register-button">Register</button>

            <button onclick="location.href='/secretary-menu'">Go Back</button>
        </div>
    </form>
    <c:if test="${notRegisteredBySecretary eq true}">
        <div>
            <p class="error"> The user was not registered yet by the secretary staff!</p>
        </div>
    </c:if>

    <c:if test="${mismatchPasswords eq true}">
        <div>
            <p class="error"> The passwords don't match!</p>
        </div>
    </c:if>

</div>
</body>
</html>
