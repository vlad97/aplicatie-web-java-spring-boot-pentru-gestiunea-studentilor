<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Catalog</title>
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>

<div class="main-container">
    <div class="table catalog-table">
        <table>
            <caption><h1>Student marks</h1></caption>
            <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Subject</th>
                <th>Mark</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${catalog}" var="catalogItem">
                <tr>
                    <td>${catalogItem.id}</td>
                    <td>${catalogItem.date}</td>
                    <td>${catalogItem.subject}</td>
                    <td>${catalogItem.mark}</td>

                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
