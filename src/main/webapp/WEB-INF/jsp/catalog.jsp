<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<html>
<head>
    <title>Catalog</title>
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <h1>Student</h1>
    <table class="table table-striped">
        <thead><tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Group number</th>
        </tr></thead>
        <tbody>
        <td>${student.firstName}</td>
        <td>${student.lastName}</td>
        <td>${student.groupNumber}</td>
        </tbody>
    </table>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Subject</th>
            <th>Mark</th>
            <th>Date</th>
        </tr>
        </thead>
        <c:forEach var="catalog" items="${catalogList}">
            <tr>
                <td>${catalog.subjectName}</td>
                <td>${catalog.mark}</td>
                <td>${catalog.markDate}</td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</div>
<button onclick="location.href='${pageContext.request.contextPath}/manage-students'">Go Back</button>
</body>
</html>
