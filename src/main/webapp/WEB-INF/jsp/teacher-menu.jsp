<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Students</title>
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="main-container">


        <div class="info">
            <h3> Main Menu</h3>

            <form action="/manage-students" method="get">
                <input type="submit" value="Manage Students"
                       name="ManageStudent" id="manageStudents" />
            </form>

            <a href="/logout">Logout</a>

        </div>


</div>
</body>
</html>
