<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<html>
<head>
    <title>AddTeacher</title>
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
<div class="main-container">

    <form method="post">
        <div class="info">
            <h3> Enter the new teacher name and email</h3>
        </div>
        <div class="add-teacher">
            <label for="firstName"><b>First name:</b></label>
            <input type="firstName" name="firstName" id="firstName" required>

            <label for="lastName"><b>Last name:</b></label>
            <input type="lastName" name="lastName" id="lastName" required>

            <label for="mail"><b>Mail Address:</b></label>
            <input type="mail" name="mail" id="mail" required>


            <button type="submit" id="add-button">Add</button>
            <button onclick="goBack()">Go Back</button>
        </div>
    </form>
    <c:if test="${teacherExists eq true}">
        <div>
            <p class="error"> The teacher is already registered!</p>
        </div>
    </c:if>

</div>
</body>
</html>
